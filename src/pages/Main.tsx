import { useContext } from "react";

import { formContext } from "context/formContext";

import { STEPS } from "data/constants";

import { getFormStep } from "features/stepper/utils/stepper";

import { Stepper } from "features/stepper/components/Stepper";
import { StepperControl } from "features/stepper/components/StepperControl";

import { isAnObjectPropertyPredicate } from "utils/predicates";

function Main() {
	const { activeStepIndex, steps, changeStep, resetFormState } =
		useContext(formContext);

	const isLastStep = activeStepIndex === STEPS.length - 1;
	const isFirstStep = activeStepIndex === 0;

	const stepsArr = Object.values(steps); 

	const activeSteps = STEPS.map( step => {
		return stepsArr.find( item => item.stepDependency === step.stepName && item.active );
	} );

	const formIsValid = activeSteps.every( step => step?.valid );

	const nextStepHandler = () => {
		changeStep({ kind: "step-payload", stepIndex: activeStepIndex + 1 });
	};

	const prevStepHandler = () => {
		changeStep({ kind: "step-payload", stepIndex: activeStepIndex - 1 });
	};

	const resetFormStateHandler = () => {
		resetFormState({
			kind: "reset-form-state-payload",
		});
	};

	const sendFormStateHandler = () => {
		const result: Record<string, Record<string, string | File>> = {};

		for(const key in steps) {
			if (!isAnObjectPropertyPredicate(steps, key)) {
				return;
			}
			const step = steps[key];
			const values = steps[key].values;

			if (!step.active) {
				continue;
			}

			result[key] = {};

			for (const valueKey in values) {
				const file = values[valueKey].file;
				result[key][valueKey] = file ? file : values[valueKey].value;
			}
		}

		// sending data here
		console.log(result);
	};

	return (
		<div className="main-wrap">
			<div className="nav">
				<div className="head">
					<h2 className="head__title">Создание аккаунта</h2>
					<p className="head__desc">
						Заполните все пункты данной формы и нажмите кнопку «Сохранить».
					</p>
				</div>
				<Stepper
					activeStepIndex={activeStepIndex}
					steps={steps}
					stepSteppers={STEPS}
					onChangeStep={changeStep}
				/>
			</div>
			<div className="content">
				{getFormStep(activeStepIndex)}
				<StepperControl
					formIsValid={formIsValid}
					isFirstStep={isFirstStep}
					isLastStep={isLastStep}
					onResetFormState={resetFormStateHandler}
					onSendFormState={sendFormStateHandler}
					onNextStep={nextStepHandler}
					onPrevStep={prevStepHandler}
				/>
			</div>
		</div>
	);
}

export { Main };
