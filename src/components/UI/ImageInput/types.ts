import { Field, StepKeys } from "context/formContext/types";

export interface ImageInputProps {
	optional: boolean;
	fieldName: string;
	inputLabel: string;
	checkboxLable?: string;
	fieldData: Field;
	stepName: StepKeys;
	onChange: (fieldName: string, value: string, file?: File) => void;
	onChangeRequired?: (fieldName: string) => void;
}