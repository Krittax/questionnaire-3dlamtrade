import { useId, useRef } from "react";

import cn from "classnames";

import { Checkbox } from "../Сheckbox";

import { ImageInputProps } from "./types";

function ImageInput({
	fieldData,
	optional,
	fieldName,
	inputLabel,
	checkboxLable,
	onChange,
	onChangeRequired,
}: ImageInputProps) {
	const inputFileRef = useRef<HTMLInputElement>(null);
	const fileInputId = useId();
	const requiredCheckboxId = useId();

	const { required, value } = fieldData;

	const trunckedValue =
		value.length > 23 ? value.slice(0, 11) + "..." + value.slice(-9) : value;

	const setImageHandler = (files: FileList | null) => {
		if (!files || !files.length) return;

		const file = files[0];
		const value = file.name;

		onChange(fieldName, value, file);
	};

	const preventFieldDragHandler = (
		event: React.DragEvent<HTMLLabelElement>
	) => {
		event.preventDefault();
		event.stopPropagation();
	};

	const dropFileHandler = (event: React.DragEvent<HTMLLabelElement>) => {
		event.preventDefault();
		event.stopPropagation();

		setImageHandler(event.dataTransfer.files);
	};

	const clearInputStateHandler = (
		event: React.MouseEvent<HTMLButtonElement, MouseEvent>
	) => {
		event.preventDefault();

		if (inputFileRef.current) {
			inputFileRef.current.value = "";
		}
		onChange(fieldName, "");
	};

	return (
		<div className="field">
			<div className="field__file">
				<span className="field__label">{inputLabel}</span>
				<input
					className="field__file-input"
					id={fileInputId}
					ref={inputFileRef}
					type="file"
					accept="image/*"
					onChange={(event) => setImageHandler(event.target.files)}
				/>
				<label
					className={cn("field__file-label", trunckedValue && "uploaded")}
					htmlFor={fileInputId}
					draggable
					onDrag={preventFieldDragHandler}
					onDragStart={preventFieldDragHandler}
					onDragEnd={preventFieldDragHandler}
					onDragOver={preventFieldDragHandler}
					onDragEnter={preventFieldDragHandler}
					onDragLeave={preventFieldDragHandler}
					onDrop={dropFileHandler}
				>
					{trunckedValue && <div className="green-mark active"></div>}
					{trunckedValue || (
						<span className="field__file-placeholder">
							Выберите или перетащите файл
						</span>
					)}
					{trunckedValue && (
						<button
							className="soc-delete"
							onClick={clearInputStateHandler}
						></button>
					)}
				</label>
			</div>
			{optional && (
				<Checkbox
					id={requiredCheckboxId}
					fieldName={fieldName}
					isRequired={!required}
					label={checkboxLable}
					onChange={onChangeRequired}
				/>
			)}
		</div>
	);
}

export { ImageInput };
