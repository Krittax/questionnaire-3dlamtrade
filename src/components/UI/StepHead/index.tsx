import { StepHeadProps } from "./types";

function StepHead({ Icon, title, description }: StepHeadProps) {
	return (
		<div className="head">
			<div className="head__img">
				<Icon />
			</div>
			<h2 className="head__title">{title}</h2>
			<p className="head__desc">
				{description}
			</p>
		</div>
	);
}

export { StepHead };
