export interface StepHeadProps {
	title: string;
	description: string;
	Icon: React.FunctionComponent<
		React.SVGProps<SVGSVGElement> & {
			title?: string | undefined;
		}
	>;
}
