import { Field } from "context/formContext/types";

interface ButtonOption {
	value: string;
	label: string;
	name: string;
}

export interface ToggleButtonProps {
	options: ButtonOption[];
	label: string;
	fieldName: string;
	onChange: (fieldName: string, value: string) => void;
	fieldData: Field;
}