import cn from "classnames";

import { ReactComponent as MaleIcon } from "assets/male.svg";
import { ReactComponent as FemaleIcon } from "assets/female.svg";

import { ToggleButtonProps } from "./types";

function ToggleButton({
	options,
	fieldData,
	label,
	fieldName,
	onChange,
}: ToggleButtonProps) {
	const { value } = fieldData;

	return (
		<div className="field field--half">
			<div className="field__wrap">
				<span className="field__label">{label}</span>
				<div className="field__radio-wrap">
					{options.map((option) => {
						const isActive = option.value === value;
						const iconName = option.name;
						let Icon = MaleIcon;
						
						if (iconName === "female") {
							Icon = FemaleIcon;
						}

						return (
							<button
								className={cn("field__radio-btn", isActive && "active")}
								key={option.value}
								onClick={() => onChange(fieldName, option.value)}
							>
								<Icon
									style={{
										marginRight: "15px",
									}}
								/>
								{option.label}
							</button>
						);
					})}
				</div>
			</div>
		</div>
	);
}

export { ToggleButton };
