import { CheckboxProps } from "./types";

function Checkbox({ id, fieldName, label, isRequired, onChange }: CheckboxProps) {
	return (
		<div className="checkbox">
			<input
				className="checkbox__input"
				type="checkbox"
				id={id}
				name="required"
				onChange={() => onChange && onChange(fieldName)}
				checked={isRequired}
			/>
			<label className="checkbox__label" htmlFor={id}>
				{label}
			</label>
		</div>
	);
}

export { Checkbox };