export interface CheckboxProps {
	id: string;
	label: string | undefined;
	fieldName: string;
	isRequired: boolean;
	onChange: ((fieldName: string) => void) | undefined;
}