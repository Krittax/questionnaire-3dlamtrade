import { Field, Payload, StepKeys } from "context/formContext/types";

export interface DatePickerProps {
	label: string;
	fieldName: string;
	onChange: (fieldName: string, value: string) => void;
	fieldData: Field;
	stepName: StepKeys;
	onTouch: (value: Payload) => void;
}