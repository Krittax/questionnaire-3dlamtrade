import cn from "classnames";

import { DatePickerProps } from "./types";

function DatePicker({
	label,
	fieldName,
	fieldData,
	stepName,
	onChange,
	onTouch,
}: DatePickerProps) {
	const { value, valid, touched, errors, errorsVisible } = fieldData;

	const hasError = (!valid && errorsVisible) || (!valid && touched);
	const isErrorsVisible = errorsVisible && errors.length > 0;

	const errorList = errors.map((error) => (
		<li key={stepName + "/" + fieldName}>{error}</li>
	));

	const blurHandler = () => {
		onTouch({
			kind: "touch-payload",
			stepName,
			fieldName,
			touch: true,
			errorsVisible: false,
		});
	};

	const focusHandler = () => {
		onTouch({
			kind: "touch-payload",
			stepName,
			fieldName,
			touch: false,
			errorsVisible: touched,
		});
	};

	return (
		<div className="field field--half">
			{isErrorsVisible && (
				<div className="field__error">
					<ul>{errorList}</ul>
				</div>
			)}
			<div className="field__wrap">
				<span className="field__label">{label}</span>
				<input
					className={cn("field__input", hasError && "invalid")}
					type="date"
					value={value}
					onChange={(event) => onChange(fieldName, event.target.value)}
					onFocus={focusHandler}
					onBlur={blurHandler}
				/>
			</div>
		</div>
	);
}

export { DatePicker };
