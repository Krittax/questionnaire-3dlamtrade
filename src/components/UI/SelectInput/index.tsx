import { useRef } from "react";
import Select, { createFilter, components } from "react-select";

import { SelectInputProps } from "./types";

function SelectInput({
	fieldData,
	stepName,
	fieldName,
	options,
	label,
	name,
	onChange,
	onTouch
}: SelectInputProps) {
	const selectRef = useRef(null);

	const { value, valid, touched, errors, errorsVisible } = fieldData;

	const hasError = (!valid && errorsVisible) || (!valid && touched);
	const isErrorsVisible = errorsVisible && errors.length > 0;

	const selectValue = value === "" ? "Выбрать" : value;

	const errorList = errors.map((error) => (
		<li key={stepName + "/" + fieldName}>{error}</li>
	));

	const blurHandler = () => {
		onTouch({
			kind: "touch-payload",
			stepName,
			fieldName,
			touch: true,
			errorsVisible: false,
		});
	};

	const focusHandler = () => {
		onTouch({
			kind: "touch-payload",
			stepName,
			fieldName,
			touch: false,
			errorsVisible: touched,
		});
	};

	return (
		<div className="field">
			{isErrorsVisible && (
				<div className="field__error">
					<ul>{errorList}</ul>
				</div>
			)}
			<div className="field__wrap">
				<span className="field__label">{label}</span>
				<Select
					ref={selectRef}
					filterOption={createFilter({ ignoreAccents: false })}
					className="basic-select"
					classNamePrefix="optimized-select"
					styles={{
						control: (baseStyles) => ({
							...baseStyles,
							borderRadius: "7px",
							borderColor: hasError ? "#FF0000" : "#E4E5E7",
						}),
						valueContainer: (baseStyles) => ({
							...baseStyles,
							padding: "2px 18px"
						}),
						singleValue: (baseStyles) => ({
							...baseStyles,
							fontSize: "15px",
							color: "#1b1b1b"
						}),
						dropdownIndicator: (baseStyles, state) => ({
							...baseStyles,
							color: "#1b1b1b",
							transform: state.isFocused ? "rotate(180deg)" : "rotate(0deg)"
						}),
					}}
					components={{
						IndicatorSeparator: () => null,
						Option: ({children, ...props}) => {
							const { onMouseMove, onMouseOver, ...rest } = props.innerProps;
							const newProps = { ...props, innerProps: rest };
							return (
								<components.Option {...newProps} className="optimized-option">
									{children}
								</components.Option>
							);
						}
					}}
					value={{ label: selectValue, value }}
					onFocus={focusHandler}
					onBlur={blurHandler}
					onChange={(option) => onChange(fieldName, option!.value)}
					name={name}
					options={options}
				/>
			</div>
		</div>
	);
}

export { SelectInput };
