import { Field, Payload, StepKeys } from "context/formContext/types";

interface SelectOption {
	value: string;
	label: string;
}

export interface SelectInputProps {
	options: SelectOption[];
	label: string;
	name: string;
	fieldName: string;
	fieldData: Field;
	stepName: StepKeys;
	onChange: (fieldName: string, value: string) => void;
	onTouch: (value: Payload) => void;
}