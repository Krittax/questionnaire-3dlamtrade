import { Field, Payload, StepKeys } from "context/formContext/types";

export interface TextInputProps {
	optional: boolean;
	fieldName: string;
	inputLabel: string;
	checkboxLable?: string;
	fieldData: Field;
	stepName: StepKeys;
	maskCharCount: number;
	placeholder?: string;
	onChange: (fieldName: string, value: string) => void;
	onTouch: (value: Payload) => void;
	onChangeRequired?: (fieldName: string) => void;
}