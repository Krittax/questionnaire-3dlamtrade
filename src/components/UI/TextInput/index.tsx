import { useId } from "react";

import cn from "classnames";

import InputMask from "react-input-mask";

import { Checkbox } from "../Сheckbox";

import { TextInputProps } from "./types";

const charRegExp = /./;

function TextInput({
	optional,
	fieldName,
	stepName,
	inputLabel,
	checkboxLable,
	fieldData,
	maskCharCount,
	placeholder,
	onChange,
	onTouch,
	onChangeRequired,
}: TextInputProps) {
	const inputId = useId();
	const checkboxId = useId();

	const { required, value, valid, touched, errors, errorsVisible } = fieldData;

	const mask = new Array(maskCharCount).fill(charRegExp);

	const hasError =
		((!valid && errorsVisible) || (!valid && touched)) && required;
	const isErrorsVisible = errorsVisible && errors.length > 0;

	const errorList = errors.map((error, index) => (
		<li key={stepName + "/" + fieldName + "/" + index}>{error}</li>
	));

	const changeValueHandler = (value: string) => {
		onChange(fieldName, value);
	};

	const blurHandler = () => {
		onTouch({
			kind: "touch-payload",
			stepName,
			fieldName,
			touch: true,
			errorsVisible: false,
		});
	};

	const focusHandler = () => {
		onTouch({
			kind: "touch-payload",
			stepName,
			fieldName,
			touch: false,
			errorsVisible: touched,
		});
	};

	let input = (
		<input
			id={inputId}
			type="text"
			className={cn("field__input", hasError && "invalid")}
			name={fieldName}
			value={value}
			placeholder={placeholder}
			onChange={(event) => changeValueHandler(event.target.value)}
			onFocus={focusHandler}
			onBlur={blurHandler}
		/>
	);

	if (maskCharCount) {
		input = (
			<InputMask
				className={cn("field__input", hasError && "invalid")}
				id={inputId}
				type="text"
				name={fieldName}
				value={value}
				onChange={(event) => changeValueHandler(event.target.value)}
				onFocus={focusHandler}
				onBlur={blurHandler}
				mask={mask}
				maskPlaceholder="x"
				alwaysShowMask
			/>
		);
	}

	return (
		<div className="field">
			{isErrorsVisible && required && (
				<div className="field__error">
					<ul>{errorList}</ul>
				</div>
			)}
			<div className="field__wrap">
				<span className="field__label">{inputLabel}</span>
				{input}
			</div>
			{optional && (
				<Checkbox
					id={checkboxId}
					fieldName={fieldName}
					isRequired={!required}
					label={checkboxLable}
					onChange={onChangeRequired}
				/>
			)}
		</div>
	);
}

export { TextInput };
