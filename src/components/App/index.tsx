import { Outlet } from "react-router-dom";

function App() {
	return (
		<div className="wrapper">
			<div className="container">
				<Outlet />
			</div>
		</div>
	);
}

export { App };
