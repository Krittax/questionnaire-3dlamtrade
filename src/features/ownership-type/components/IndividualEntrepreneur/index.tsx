import { useContext } from "react";

import { formContext } from "context/formContext";

import { TextInput } from "components/UI/TextInput";
import { ImageInput } from "components/UI/ImageInput";
import { DatePicker } from "components/UI/DatePicker";

function IndividualEntrepreneur() {
	const { steps, changeValue, setTouch, changeRequired } =
		useContext(formContext);

	const {
		inn,
		scanInn,
		registrationDate,
		ogrnip,
		scanOgrnip,
		scanLeaseAgreement,
		scanExtractEgrip,
	} = steps.individualEntrepreneur.values;

	const changeValueHandler = (
		fieldName: string,
		value: string,
		file?: File
	) => {
		changeValue({
			kind: "value-payload",
			stepName: "individualEntrepreneur",
			fieldName,
			value,
			file,
		});
	};

	const changeRequiredHandler = (fieldName: string) => {
		changeRequired({
			kind: "required-payload",
			stepName: "individualEntrepreneur",
			fieldName,
		});
	};

	return (
		<div className="field-row">
			<div className="field-col full row triple">
				<TextInput
					optional={false}
					fieldData={inn}
					fieldName="inn"
					stepName="individualEntrepreneur"
					inputLabel="ИНН*"
					maskCharCount={10}
					onChange={changeValueHandler}
					onTouch={setTouch}
				/>
				<ImageInput
					optional={false}
					fieldData={scanInn}
					fieldName="scanInn"
					stepName="individualEntrepreneur"
					inputLabel="Скан ИНН*"
					onChange={changeValueHandler}
				/>
				<DatePicker
					fieldData={registrationDate}
					fieldName="registrationDate"
					stepName="individualEntrepreneur"
					label="Дата регистрации*"
					onChange={changeValueHandler}
					onTouch={setTouch}
				/>
			</div>
			<div className="field-col">
				<TextInput
					optional={false}
					fieldData={ogrnip}
					fieldName="ogrnip"
					stepName="individualEntrepreneur"
					inputLabel="ОГРНИП*"
					maskCharCount={15}
					onChange={changeValueHandler}
					onTouch={setTouch}
				/>
			</div>
			<div className="field-col">
				<ImageInput
					optional={false}
					fieldData={scanOgrnip}
					fieldName="scanOgrnip"
					stepName="individualEntrepreneur"
					inputLabel="Скан ОГРНИП*"
					onChange={changeValueHandler}
				/>
			</div>
			<div className="field-col col">
				<ImageInput
					optional={true}
					fieldData={scanLeaseAgreement}
					fieldName="scanLeaseAgreement"
					stepName="individualEntrepreneur"
					inputLabel="Скан договора аренды помещения (офиса)"
					checkboxLable="Нет договора"
					onChange={changeValueHandler}
					onChangeRequired={changeRequiredHandler}
				/>
			</div>
			<div className="field-col">
				<ImageInput
					optional={false}
					fieldData={scanExtractEgrip}
					fieldName="scanExtractEgrip"
					stepName="individualEntrepreneur"
					inputLabel="Скан выписки из ЕГРИП (не старше 3 месяцев)*"
					onChange={changeValueHandler}
				/>
			</div>
		</div>
	);
}

export { IndividualEntrepreneur };
