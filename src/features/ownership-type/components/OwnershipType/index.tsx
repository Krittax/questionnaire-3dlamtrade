import { useContext, useState } from "react";

import Select, { components } from "react-select";

import { formContext } from "context/formContext";

import { OWNERSHIP_TYPE } from "data/constants";

import { IndividualEntrepreneur } from "../IndividualEntrepreneur";
import { LimitedLiabilityCompany } from "../LimitedLiabilityCompany";
import { StepHead } from "components/UI/StepHead";

import { ReactComponent as OwnershipIcon } from "assets/ownership.svg";

function OwnershipType() {
	const {
		steps: { individualEntrepreneur, limitedLiabilityCompany },
		changeActiveForm,
	} = useContext(formContext);

	const forms = [individualEntrepreneur, limitedLiabilityCompany];
	const activeFormType = forms.find((form) => form.active)?.type;
	const initFormType = OWNERSHIP_TYPE.find(
		(form) => form.stepName === activeFormType
	);

	const [type, setType] = useState(initFormType || null);

	const changeOwnershipTypeHandler = (
		type: { value: string; label: string; stepName: string } | null
	) => {
		if (type === null) return;

		setType(type);
		changeActiveForm({
			kind: "form-type-payload",
			activeStepName: type.stepName,
			disableStepName:
				type.stepName === "individualEntrepreneur"
					? "limitedLiabilityCompany"
					: "individualEntrepreneur",
		});
	};

	let form: React.ReactNode;

	if (type?.stepName === "individualEntrepreneur") {
		form = <IndividualEntrepreneur />;
	}
	if (type?.stepName === "limitedLiabilityCompany") {
		form = <LimitedLiabilityCompany />;
	}

	return (
		<>
			<StepHead
				Icon={OwnershipIcon}
				title="Форма собственности"
				description="Выберите форму собственности и заполните данные"
			/>
			<div className="own-wrap">
				<Select
					className="basic-single"
					classNamePrefix="select"
					placeholder="Выбрать"
					styles={{
						control: (baseStyles) => ({
							...baseStyles,
							borderRadius: "7px",
							borderColor: "#E4E5E7",
							maxWidth: "521px",
						}),
						valueContainer: (baseStyles) => ({
							...baseStyles,
							padding: "2px 18px",
						}),
						singleValue: (baseStyles) => ({
							...baseStyles,
							fontSize: "15px",
							color: "#1b1b1b",
						}),
						dropdownIndicator: (baseStyles, state) => ({
							...baseStyles,
							color: "#1b1b1b",
							transform: state.isFocused ? "rotate(180deg)" : "rotate(0deg)",
						}),
					}}
					components={{
						IndicatorSeparator: () => null,
						Option: ({children, ...props}) => {
							const { onMouseMove, onMouseOver, ...rest } = props.innerProps;
							const newProps = { ...props, innerProps: rest };
							return (
								<components.Option {...newProps} className="optimized-option">
									{children}
								</components.Option>
							);
						}
					}}
					
					isSearchable={false}
					value={type}
					onChange={(option) => changeOwnershipTypeHandler(option)}
					name="ownership-type"
					options={OWNERSHIP_TYPE}
				/>
			</div>
			{form}
		</>
	);
}

export { OwnershipType };
