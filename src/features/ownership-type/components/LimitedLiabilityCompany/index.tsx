import { useContext, useEffect, useState } from "react";

import { formContext } from "context/formContext";

import { getInnInformation } from "api/services/getInnInformation";

import { TextInput } from "components/UI/TextInput";
import { ImageInput } from "components/UI/ImageInput";
import { DatePicker } from "components/UI/DatePicker";

function LimitedLiabilityCompany() {
	const [notificationVisible, setNotificationVisible] = useState(true);
	const { steps, changeValue, setTouch } = useContext(formContext);

	const {
		fullName,
		shortName,
		registrationDate,
		inn,
		scanInn,
		ogrn,
		scanOgrn,
	} = steps.limitedLiabilityCompany.values;

	const stepValues: string[] = [];

	for (const fieldName in steps.limitedLiabilityCompany.values) {
		if (fieldName === "inn" || fieldName.includes("scan")) {
			stepValues.push("");
		} else {
			stepValues.push(steps.limitedLiabilityCompany.values[fieldName].value);
		}
	}
	const valuesIsEmpty = stepValues.every((value) => value === "");

	useEffect(() => {
		const timerId = setTimeout(async () => {
			if (inn.valid && valuesIsEmpty) {
				const result = await getInnInformation(inn.value);

				for (const prop in result) {
					const value = result[prop];

					changeValue({
						kind: "value-payload",
						stepName: "limitedLiabilityCompany",
						fieldName: prop,
						value,
					});
				}
			}
		}, 1000);

		return () => {
			clearTimeout(timerId);
		};
	}, [inn.valid, inn.value, valuesIsEmpty, changeValue]);

	const changeValueHandler = (
		fieldName: string,
		value: string,
		file?: File
	) => {
		changeValue({
			kind: "value-payload",
			stepName: "limitedLiabilityCompany",
			fieldName,
			value,
			file,
		});
	};

	const closeNotificationHandler = () => {
		setNotificationVisible(false);
	};

	return (
		<div className="field-row">
			<div className="field-col full row double">
				<TextInput
					optional={false}
					fieldData={fullName}
					fieldName="fullName"
					stepName="limitedLiabilityCompany"
					inputLabel="Наименование полное*"
					placeholder="ООО «Московская промышленная компания»"
					maskCharCount={0}
					onChange={changeValueHandler}
					onTouch={setTouch}
				/>
				<TextInput
					optional={false}
					fieldData={shortName}
					fieldName="shortName"
					stepName="limitedLiabilityCompany"
					inputLabel="Сокращение*"
					placeholder="ООО «МПК»"
					maskCharCount={0}
					onChange={changeValueHandler}
					onTouch={setTouch}
				/>
			</div>
			<div className="field-col full own">
				<DatePicker
					fieldData={registrationDate}
					fieldName="registrationDate"
					stepName="limitedLiabilityCompany"
					label="Дата регистрации*"
					onChange={changeValueHandler}
					onTouch={setTouch}
				/>
				<div className="field field-autofill">
					{notificationVisible && (
						<div className="field__notification">
							<span>
								Начните вводить сначала ИНН и информация о компании заполнится
								автоматически
							</span>
							<button onClick={closeNotificationHandler}></button>
						</div>
					)}
					<TextInput
						optional={false}
						fieldData={inn}
						fieldName="inn"
						stepName="limitedLiabilityCompany"
						inputLabel="ИНН*"
						maskCharCount={10}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<ImageInput
					optional={false}
					fieldData={scanInn}
					fieldName="scanInn"
					stepName="limitedLiabilityCompany"
					inputLabel="Скан ИНН*"
					onChange={changeValueHandler}
				/>
			</div>
			<div className="field-col full own">
				<TextInput
					optional={false}
					fieldData={ogrn}
					fieldName="ogrn"
					stepName="limitedLiabilityCompany"
					inputLabel="ОГРН*"
					maskCharCount={13}
					onChange={changeValueHandler}
					onTouch={setTouch}
				/>
				<ImageInput
					optional={false}
					fieldData={scanOgrn}
					fieldName="scanOgrn"
					stepName="limitedLiabilityCompany"
					inputLabel="Скан ОГРН*"
					onChange={changeValueHandler}
				/>
			</div>
		</div>
	);
}

export { LimitedLiabilityCompany };
