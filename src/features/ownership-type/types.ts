interface CompanyData {
	data: {
		ogrn: string;
		state: {
			registration_date: number;
		};
		name: {
			full_with_opf: string;
			short_with_opf: string;
		};
	};
}

export interface Company {
	suggestions: CompanyData[]
}