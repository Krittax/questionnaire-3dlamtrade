import { PersonalInformation } from "features/personal-information";
import { OwnershipType } from "features/ownership-type";
import { RegistrationAddress } from "features/registration-address";
import { ResidentialAddress } from "features/residential-address";
import { SocialMedia } from "features/social-media";

export function getFormStep(stepIndex: number) {
	switch (stepIndex) {
		case 0:
			return <PersonalInformation />;
		case 1:
			return <OwnershipType />;
		case 2:
			return <RegistrationAddress />;
		case 3:
			return <ResidentialAddress />;
		case 4:
			return <SocialMedia />;
	}
}