import cn from "classnames";

import { StepperControlProps } from "features/stepper/types";

function StepperControl({
	isFirstStep,
	isLastStep,
	formIsValid,
	onNextStep,
	onPrevStep,
	onResetFormState,
	onSendFormState
}: StepperControlProps) {
	return (
		<div className="button-wrap">
			<button
				className="back-btn"
				onClick={isFirstStep ? onResetFormState : onPrevStep}
			>
				{isFirstStep ? "Отмена" : "Назад"}
			</button>
			<button
				className={cn(
					"btn",
					"primary",
					!formIsValid && isLastStep && "disabled"
				)}
				disabled={!formIsValid && isLastStep}
				onClick={isLastStep ? onSendFormState : onNextStep}
			>
				{isLastStep ? "Сохранить" : "Далее"}
			</button>
		</div>
	);
}

export { StepperControl };