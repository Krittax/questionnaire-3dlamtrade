import cn from "classnames";

import { StepperProps } from "features/stepper/types";

function Stepper({ stepSteppers, steps, activeStepIndex, onChangeStep }: StepperProps ) {
	return (
		<div className="nav__list">
			{stepSteppers.map((step, index) => {
				const dependencies = step.value;
				const targetStep = Object.values(steps).find((step) => {
					return dependencies.includes(step.type) && step.active;
				});

				return (
					<button
						key={step.stepName}
						type="button"
						className={cn(
							"nav__btn",
							targetStep?.valid && "done",
							activeStepIndex === index && "active"
						)}
						onClick={() =>
							onChangeStep({ kind: "step-payload", stepIndex: index })
						}
					>
						<span className="nav__number">{index + 1}</span>
						<span className="nav__text">{step.label}</span>
						<div className="nav__icon"></div>
					</button>
				);
			})}
		</div>
	);
}

export { Stepper };