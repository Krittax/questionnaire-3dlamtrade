import { StepStepper, FormState, Payload } from "context/formContext/types";

export interface StepperProps {
	stepSteppers: StepStepper[];
	steps: FormState["steps"];
	activeStepIndex: number;
	onChangeStep: (value: Payload) => void;
}

export interface StepperControlProps {
	isFirstStep: boolean;
	isLastStep: boolean;
	formIsValid: boolean;
	onResetFormState: () => void;
	onSendFormState: () => void;
	onPrevStep: () => void;
	onNextStep: () => void;
}