import { ReactComponent as PersonalIcon } from "assets/personal.svg";

import { useContext } from "react";

import { formContext } from "context/formContext";

import { TextInput } from "components/UI/TextInput";
import { SelectInput } from "components/UI/SelectInput";
import { ToggleButton } from "components/UI/ToggleButton";
import { DatePicker } from "components/UI/DatePicker";
import { StepHead } from "components/UI/StepHead";

import cities from "data/cities.json";
import countries from "data/countries.json";
import { GENDERS } from "data/constants";

function PersonalInformation() {
	const { steps, changeValue, setTouch } = useContext(formContext);

	const {
		surname,
		firstname,
		patronymic,
		city,
		nationality,
		gender,
		birthDate,
		birthPlace,
	} = steps.personalInformation.values;

	const changeValueHandler = (fieldName: string, value: string) => {
		changeValue({
			kind: "value-payload",
			stepName: "personalInformation",
			fieldName,
			value,
		});
	};

	return (
		<div>
			<StepHead
				Icon={PersonalIcon}
				title="Создание аккаунта"
				description="Заполните все пункты данной формы и нажмите кнопку «Сохранить»."
			/>
			<div className="field-row">
				<div className="field-col">
					<TextInput
						optional={false}
						fieldData={surname}
						fieldName="surname"
						stepName="personalInformation"
						inputLabel="Фамилия*"
						placeholder="Васильев"
						maskCharCount={0}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col">
					<TextInput
						optional={false}
						fieldData={firstname}
						fieldName="firstname"
						stepName="personalInformation"
						inputLabel="Имя*"
						placeholder="Иван"
						maskCharCount={0}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col">
					<TextInput
						optional={false}
						fieldData={patronymic}
						fieldName="patronymic"
						stepName="personalInformation"
						inputLabel="Отчество*"
						placeholder="Сергеевич"
						maskCharCount={0}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col">
					<SelectInput
						fieldData={city}
						stepName="personalInformation"
						fieldName="city"
						label="Основной город*"
						name="cities"
						options={cities}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col">
					<SelectInput
						fieldData={nationality}
						stepName="personalInformation"
						fieldName="nationality"
						label="Гражданство*"
						name="nationalities"
						options={countries}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col row">
					<ToggleButton
						fieldData={gender}
						fieldName="gender"
						label="Пол*"
						options={GENDERS}
						onChange={changeValueHandler}
					/>
					<DatePicker
						fieldData={birthDate}
						stepName="personalInformation"
						fieldName="birthDate"
						label="Дата рождения*"
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col full">
					<TextInput
						optional={false}
						fieldData={birthPlace}
						fieldName="birthPlace"
						stepName="personalInformation"
						inputLabel="Место рождения (как указано в паспорте)*"
						placeholder="Введите наименование региона и населенного пункта"
						maskCharCount={0}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
			</div>
		</div>
	);
}

export { PersonalInformation };
