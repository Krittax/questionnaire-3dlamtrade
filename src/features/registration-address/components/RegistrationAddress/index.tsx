import { useContext } from "react";

import { formContext } from "context/formContext";

import { TextInput } from "components/UI/TextInput";
import { SelectInput } from "components/UI/SelectInput";
import { DatePicker } from "components/UI/DatePicker";
import { StepHead } from "components/UI/StepHead";

import countries from "data/countries.json";
import regions from "data/regions.json";

import { ReactComponent as RegistrationIcon } from "assets/registration.svg";

function RegistrationAddress() {
	const { steps, changeValue, setTouch, changeRequired } = useContext(formContext);

	const {
		country,
		region,
		city,
		street,
		house,
		apartment,
		registrationDate,
	} = steps.registrationAddress.values;

	const changeValueHandler = (fieldName: string, value: string) => {
		changeValue({
			kind: "value-payload",
			stepName: "registrationAddress",
			fieldName,
			value,
		});
	};

	const changeRequiredHandler = (fieldName: string) => {
		changeRequired({
			kind: "required-payload",
			stepName: "registrationAddress",
			fieldName,
		});
	};

	return (
		<div>
			<StepHead
				Icon={RegistrationIcon}
				title="Адрес регистрации"
				description="Введите свой действуйющий адрес прописки."
			/>
			<div className="field-row">
				<div className="field-col">
					<SelectInput
						fieldData={country}
						stepName="registrationAddress"
						fieldName="country"
						label="Страна*"
						name="countries"
						options={countries}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col">
					<SelectInput
						fieldData={region}
						stepName="registrationAddress"
						fieldName="region"
						label="Регион*"
						name="regions"
						options={regions}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col">
					<TextInput
						optional={false}
						fieldData={city}
						fieldName="city"
						stepName="registrationAddress"
						inputLabel="Город / Населенный пункт*"
						placeholder="Введите населенный пункт"
						maskCharCount={0}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col">
					<TextInput
						optional={false}
						fieldData={street}
						fieldName="street"
						stepName="registrationAddress"
						inputLabel="Улица*"
						placeholder="Введите улицу"
						maskCharCount={0}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col small">
					<TextInput
						optional={false}
						fieldData={house}
						fieldName="house"
						stepName="registrationAddress"
						inputLabel="Дом*"
						placeholder="0"
						maskCharCount={0}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
					<TextInput
						optional={true}
						fieldData={apartment}
						fieldName="apartment"
						stepName="registrationAddress"
						inputLabel="Квартира*"
						checkboxLable="Нет квартиры"
						placeholder="0"
						maskCharCount={0}
						onChange={changeValueHandler}
						onTouch={setTouch}
						onChangeRequired={changeRequiredHandler}
					/>
				</div>
				<div className="field-col">
					<DatePicker
						fieldData={registrationDate}
						fieldName="registrationDate"
						stepName="registrationAddress"
						label="Дата прописки*"
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
			</div>
		</div>
	);
}

export { RegistrationAddress };
