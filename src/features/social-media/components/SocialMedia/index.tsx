import { useContext } from "react";

import Select, { components } from "react-select";
import cn from "classnames";

import { formContext } from "context/formContext";

import socials from "data/socials.json";

import { StepHead } from "components/UI/StepHead";

import { ReactComponent as SocailIcon } from "assets/social.svg";
import { ReactComponent as Cross } from "assets/cross.svg";

function SocialMedia() {
	const { steps, changeValue, addSocial, deleteSocial } =
		useContext(formContext);

	const socialKeys = Object.keys(steps.socialMedia.values);

	const sortedSocials = socialKeys.sort((a, b) => {
		return (
			steps.socialMedia.values[a].order - steps.socialMedia.values[b].order
		);
	});

	const filteredSocials = socials.filter(
		(social) => !socialKeys.includes(social.name)
	);

	const addSocialFieldHandler = () => {
		addSocial({
			kind: "add-social-payload",
			stepName: "socialMedia",
			oldSocialName: "",
			newSocialName: "default",
			socialValue: "",
		});
	};

	const addSocialMediaHandler = (
		newSocialName: string,
		oldSocialName: string
	) => {
		const existingSocial = steps.socialMedia.values[newSocialName];

		addSocial({
			kind: "add-social-payload",
			stepName: "socialMedia",
			oldSocialName,
			newSocialName,
			socialValue: existingSocial ? existingSocial.value : "",
		});
	};

	const changeValueHandler = (fieldName: string, value: string) => {
		changeValue({
			kind: "value-payload",
			stepName: "socialMedia",
			fieldName,
			value,
		});
	};

	const deleteSocialHandler = (fieldName: string) => {
		deleteSocial({
			kind: "delete-social-payload",
			stepName: "socialMedia",
			fieldName,
		});
	};

	return (
		<div>
			<StepHead
				Icon={SocailIcon}
				title="Социальные сети"
				description="Введите свои действуйющие ссылки на социальные сети и количество
					подписчиков."
			/>
			{sortedSocials.map((fieldName) => {
				const foundSocial = socials.find((social) => social.name === fieldName);
				let selectValue = {
					value: "",
					placeholder: "",
					name: "",
					label: "",
				};

				if (foundSocial) {
					selectValue = {
						value: foundSocial.value,
						placeholder: foundSocial.placeholder,
						name: foundSocial.name,
						label: foundSocial.label,
					};
				}

				let hasError = false;

				if (steps.socialMedia.values[selectValue.name]) {
					hasError = !steps.socialMedia.values[selectValue.name].valid;
				}

				return (
					<div key={fieldName} className="soc-wrap">
						<Select
							className="basic-single"
							classNamePrefix="select"
							name="socials"
							isSearchable={false}
							options={filteredSocials}
							styles={{
								control: (baseStyles) => ({
									...baseStyles,
									borderRadius: "7px",
									borderColor: "#E4E5E7",
									minWidth: "229px",
								}),
								valueContainer: (baseStyles) => ({
									...baseStyles,
									padding: "2px 18px",
								}),
								singleValue: (baseStyles) => ({
									...baseStyles,
									fontSize: "15px",
									color: "#1b1b1b",
								}),
								dropdownIndicator: (baseStyles, state) => ({
									...baseStyles,
									color: "#1b1b1b",
									transform: state.isFocused
										? "rotate(180deg)"
										: "rotate(0deg)",
								}),
							}}
							components={{
								IndicatorSeparator: () => null,
								Option: ({children, ...props}) => {
									const { onMouseMove, onMouseOver, ...rest } = props.innerProps;
									const newProps = { ...props, innerProps: rest };
									return (
										<components.Option {...newProps} className="optimized-option">
											{children}
										</components.Option>
									);
								}
							}}
							value={{
								value: selectValue.value,
								placeholder: selectValue.placeholder,
								name: selectValue.name,
								label: selectValue.label || "Выбрать",
							}}
							onChange={(option) =>
								addSocialMediaHandler(
									option!.name,
									foundSocial ? selectValue.name : "default"
								)
							}
						/>
						{selectValue.name && (
							<>
								<input
									className={cn("soc-input", hasError && "invalid")}
									type="text"
									placeholder={selectValue.placeholder}
									value={steps.socialMedia.values[fieldName].value}
									onChange={(event) =>
										changeValueHandler(fieldName, event.target.value)
									}
								/>
								<button
									className="soc-delete"
									onClick={() => deleteSocialHandler(selectValue.name)}
								></button>
							</>
						)}
					</div>
				);
			})}
			{filteredSocials.length !== 0 && (
				<button
					className={cn(
						"add-btn",
						socialKeys.includes("default") && "disabled"
					)}
					disabled={socialKeys.includes("default")}
					onClick={addSocialFieldHandler}
				>
					<Cross />
					Добавить социальную сеть
				</button>
			)}
		</div>
	);
}

export { SocialMedia };
