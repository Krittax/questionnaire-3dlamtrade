import { useContext, useId } from "react";

import { formContext } from "context/formContext";

import { TextInput } from "components/UI/TextInput";
import { SelectInput } from "components/UI/SelectInput";
import { DatePicker } from "components/UI/DatePicker";
import { Checkbox } from "components/UI/Сheckbox";
import { StepHead } from "components/UI/StepHead";

import countries from "data/countries.json";
import regions from "data/regions.json";

import { ReactComponent as ResidentialIcon } from "assets/residential.svg";

function ResidentialAddress() {
	const addressesCheckboxId = useId();
	const { steps, changeValue, setTouch, changeRequired, copyStepState } = useContext(formContext);

	const {
		country,
		region,
		city,
		street,
		house,
		apartment,
		registrationDate,
	} = steps.residentialAddress.values;

	const changeValueHandler = (fieldName: string, value: string) => {
		changeValue({
			kind: "value-payload",
			stepName: "residentialAddress",
			fieldName,
			value,
		});
	};

	const changeRequiredHandler = (fieldName: string) => {
		changeRequired({
			kind: "required-payload",
			stepName: "residentialAddress",
			fieldName,
		});
	};

	const copyStepStateHandler = () => {
		copyStepState({
			kind: "copy-step-state",
			originStepName: "registrationAddress",
			destinationStepName: "residentialAddress"
		});
	};

	return (
		<div>
			<StepHead
				Icon={ResidentialIcon}
				title="Адрес проживания"
				description="Введите свой действуйющий адрес проживания."
			/>
			<div className="field-row">
				<div className="field-col full">
					<Checkbox
						id={addressesCheckboxId}
						isRequired={steps.residentialAddress.copied}
						fieldName=""
						label="Адрес регистрации и фактического проживания совпадают"
						onChange={copyStepStateHandler}
					/>
				</div>
				<div className="field-col">
					<SelectInput
						fieldData={country}
						stepName="residentialAddress"
						fieldName="country"
						label="Страна*"
						name="countries"
						options={countries}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col">
					<SelectInput
						fieldData={region}
						stepName="residentialAddress"
						fieldName="region"
						label="Регион*"
						name="regions"
						options={regions}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col">
					<TextInput
						optional={false}
						fieldData={city}
						fieldName="city"
						stepName="residentialAddress"
						inputLabel="Город / Населенный пункт*"
						placeholder="Введите населенный пункт"
						maskCharCount={0}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col">
					<TextInput
						optional={false}
						fieldData={street}
						fieldName="street"
						stepName="residentialAddress"
						inputLabel="Улица*"
						placeholder="Введите улицу"
						maskCharCount={0}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
				<div className="field-col small">
					<TextInput
						optional={false}
						fieldData={house}
						fieldName="house"
						stepName="residentialAddress"
						inputLabel="Дом*"
						placeholder="0"
						maskCharCount={0}
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>

					<TextInput
						optional={true}
						fieldData={apartment}
						fieldName="apartment"
						stepName="residentialAddress"
						inputLabel="Квартира*"
						checkboxLable="Нет квартиры"
						placeholder="0"
						maskCharCount={0}
						onChange={changeValueHandler}
						onTouch={setTouch}
						onChangeRequired={changeRequiredHandler}
					/>
				</div>
				<div className="field-col">
					<DatePicker
						fieldData={registrationDate}
						fieldName="registrationDate"
						stepName="residentialAddress"
						label="Дата прописки*"
						onChange={changeValueHandler}
						onTouch={setTouch}
					/>
				</div>
			</div>
		</div>
	);
}

export { ResidentialAddress };
