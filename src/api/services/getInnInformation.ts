import { DADATA_API_KEY } from "data/constants";

import { getFormattedDate } from "features/ownership-type/utils/date-formatters";

import { Company } from "features/ownership-type/types";

export function getInnInformation(inn: string) {
	return fetch(
		"https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/party",
		{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Accept: "application/json",
				Authorization: "Token " + DADATA_API_KEY,
			},
			body: JSON.stringify({ query: inn, branch_type: "MAIN" }),
		}
	)
		.then((response) => response.json())
		.then((result) => {
			const companyData = (result as Company).suggestions[0]?.data;

			if (!companyData) return;

			const convertedResult = {
				fullName: companyData.name.full_with_opf,
				shortName: companyData.name.short_with_opf,
				registrationDate: getFormattedDate(companyData.state.registration_date),
				ogrn: companyData.ogrn,
			} as Record<string, string>;

			return convertedResult;
		});
}