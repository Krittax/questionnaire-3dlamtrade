import { PERSONAL_INFORMATION } from './stepSlices/personalInformation';
import { LIMITED_LIABILITY_COMPANY, INDIVIDUAL_ENTERPRENEUR } from './stepSlices/ownershipType';
import { REGISTRATION_ADDRESS } from './stepSlices/registrationAddress';
import { RESIDENTIAL_ADDRESS } from './stepSlices/residentialAddress'; 
import { SOCIAL_MEDIA } from './stepSlices/socialMedia';

import { FormState, StepStepper } from "context/formContext/types";

export const FORM_STATE: FormState = {
	activeStepIndex:0,
	steps: {
		personalInformation: PERSONAL_INFORMATION,
		individualEntrepreneur: INDIVIDUAL_ENTERPRENEUR,
		limitedLiabilityCompany: LIMITED_LIABILITY_COMPANY,
		registrationAddress: REGISTRATION_ADDRESS,
		residentialAddress: RESIDENTIAL_ADDRESS,
		socialMedia: SOCIAL_MEDIA,
	},
};

export const STEPS: StepStepper[] = [
	{
		stepName: "personalInformation",
		value: ["personalInformation"],
		label: "Общие",
	},
	{
		stepName: "ownershipType",
		value: ["individualEntrepreneur", "limitedLiabilityCompany"],
		label: "Форма собственности",
	},
	{
		stepName: "registrationAddress",
		value: ["registrationAddress"],
		label: "Адрес регистрации",
	},
	{
		stepName: "residentialAddress",
		value: ["residentialAddress"],
		label: "Адрес проживания",
	},
	{
		stepName: "socialMedia",
		value: ["socialMedia"],
		label: "Социальные сети",
	},
];

export const GENDERS = [
	{ value: "Мужской", label: "М", name: "male" },
	{ value: "Женский", label: "Ж", name: "female" },
];

export const OWNERSHIP_TYPE = [
	{
		stepName: "individualEntrepreneur",
		label: "Индивидуальный предприниматель (ИП)",
		value: "Индивидуальный предприниматель (ИП)",
	},
	{
		stepName: "limitedLiabilityCompany",
		label: "Общество с ограниченной ответственностью (ООО)",
		value: "Общество с ограниченной ответственностью (ООО)",
	},
];

export const DADATA_API_KEY = "351f6bfeadcabe933a78e46ea5d7df963e24755a";

export enum ActionKind {
	changeValue = "CHANGE_VALUE",
	changeStep = "CHANGE_STEP",
	setTouch = "SET_TOUCH",
	changeRequired = "CHANGE_REQUIRED",
	changeFormType = "CHANGE_FORM_TYPE",
	copyStepState = "COPY_STEP_STATE",
	addSocial = "ADD_SOCIAL",
	resetFormState = "RESET_FORM_STATE",
	deleteSocial = "DELETE_SOCIAL"
}