import { CHECKS } from "data/checks";

import { Step } from "context/formContext/types";

export const SOCIAL_MEDIA: Step = {
	type: "socialMedia",
	stepDependency: "socialMedia",
	active: true,
	copied: false,
	valid: false,
	values: {
		default: {
			required: true,
			value: "",
			file: null,
			order: 0,
			valid: false,
			touched: false,
			errorsVisible: false,
			errors: [],
			checks: [CHECKS.emptyCheck],
		}
	},
};