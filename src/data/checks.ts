export const CHECKS = {
	emptyCheck: {
		regexp: /^.+$/i,
		errorMessage: "Поле обязательно к заполнению",
	},
	onlyLettersCheck: {
		regexp: /^[a-zа-я ]+$/i,
		errorMessage: "Допустимы только кириллические и латинские символы",
	},
	onlyNumbersCheck: {
		regexp: /^[0-9]+$/i,
		errorMessage: "Допустимы только цифры",
	},
	dateFormatCheck: {
		regexp: /^\d{4}-\d{2}-\d{2}$/i,
		errorMessage: "Некорректный формат даты",
	},
	companyNameCheck: {
		regexp: /^[a-zа-я«»"' ]+$/i,
		errorMessage: "Недопустимые символы",
	},
};