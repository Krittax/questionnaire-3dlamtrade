import React from "react";
import ReactDOM from "react-dom/client";

import { RouterProvider } from "react-router-dom";

import { FormContextProvider } from "context/formContext";

import { router } from "routes";

import "normalize.css";
import "styles/index.scss";
import "styles/checkbox.scss";
import "styles/step-head.scss";
import "styles/field.scss";
import "styles/buttons.scss";
import "styles/nav.scss";
import "styles/soical.scss";
import "styles/select.scss";

const root = ReactDOM.createRoot(
	document.getElementById("root") as HTMLDivElement
);

root.render(
	<React.StrictMode>
		<FormContextProvider>
			<RouterProvider router={router} />
		</FormContextProvider>
	</React.StrictMode>
);
