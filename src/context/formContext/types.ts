import { ActionKind } from "data/constants";

export interface FormProviderProps {
	children: React.ReactNode;
}

interface Check {
	regexp: RegExp;
	errorMessage: string;
}

export interface Field {
	required: boolean;
	value: string;
	file: File | null;
	valid: boolean;
	order: number;
	touched: boolean;
	errorsVisible: boolean;
	errors: string[];
	checks: Check[];
}

export interface Step {
	type: StepKeys;
	stepDependency: string;
	active: boolean;
	valid: boolean;
	copied: boolean;
	values: Record<string, Field>;
}

export interface FormState {
	activeStepIndex: number;
	steps: {
		personalInformation: Step;
		individualEntrepreneur: Step;
		limitedLiabilityCompany: Step;
		registrationAddress: Step;
		residentialAddress: Step;
		socialMedia: Step;
	};
}

export type StepKeys = keyof FormState["steps"];

export interface StepStepper {
	stepName: string;
	value: StepKeys[];
	label: string;
}

interface ValuePayload {
	kind: "value-payload";
	stepName: StepKeys;
	fieldName: string;
	value: string;
	file?: File;
}

interface TouchPayload {
	kind: "touch-payload";
	stepName: StepKeys;
	fieldName: string;
	touch: boolean;
	errorsVisible: boolean;
}

interface RequiredPayload {
	kind: "required-payload";
	stepName: StepKeys;
	fieldName: string;
}

interface FormTypePayload {
	kind: "form-type-payload";
	activeStepName: string;
	disableStepName: string;
}

interface StepPayload {
	kind: "step-payload";
	stepIndex: number;
}

interface CopyStepStatePayload {
	kind: "copy-step-state";
	originStepName: StepKeys;
	destinationStepName: StepKeys;
}

interface AddSocialPayload {
	kind: "add-social-payload",
	stepName: StepKeys;
	oldSocialName: string;
	newSocialName: string;
	socialValue: string;
}

interface RenameFieldNamePayload {
	kind: "rename-field-name-payload",
	stepName: StepKeys;
	oldName: string;
	newName: string;
}

interface ResetFormStatePayload {
	kind: "reset-form-state-payload"
}

interface DeleteSocialPayload {
	kind: "delete-social-payload",
	stepName: StepKeys,
	fieldName: string;
}

export type Payload =
	| ValuePayload
	| TouchPayload
	| RequiredPayload
	| StepPayload
	| FormTypePayload
	| CopyStepStatePayload
	| AddSocialPayload
	| RenameFieldNamePayload
	| ResetFormStatePayload
	| DeleteSocialPayload;

export interface Action {
	type: ActionKind;
	payload: Payload;
}

export type ActionCreator = (payload: Payload) => Action;