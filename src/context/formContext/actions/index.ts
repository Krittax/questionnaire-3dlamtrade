import { ActionKind } from "data/constants";

import { ActionCreator } from "../types";

export const changeValueAction: ActionCreator = (payload) => {
	return { type: ActionKind.changeValue, payload };
};

export const changeStepAction: ActionCreator = (payload) => {
	return { type: ActionKind.changeStep, payload };
};

export const changeRequiredAction: ActionCreator = (payload) => {
	return { type: ActionKind.changeRequired, payload };
};

export const changeFormTypeAction: ActionCreator = (payload) => {
	return { type: ActionKind.changeFormType, payload };
};

export const copyStepStateAction: ActionCreator = (payload) => {
	return { type: ActionKind.copyStepState, payload };
};

export const addSocialAction: ActionCreator = (payload) => {
	return { type: ActionKind.addSocial, payload };
};

export const resetFormStateAction: ActionCreator = (payload) => {
	return { type: ActionKind.resetFormState, payload };
};

export const deleteSocialAciton: ActionCreator = (payload) => {
	return { type: ActionKind.deleteSocial, payload };
};

export const setTouchAction: ActionCreator = (payload) => {
	return { type: ActionKind.setTouch, payload };
};