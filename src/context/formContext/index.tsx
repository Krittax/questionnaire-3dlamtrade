import { createContext, useReducer, useCallback } from "react";

import { reducer } from "./reducer";
import {
	changeValueAction,
	changeStepAction,
	setTouchAction,
	changeRequiredAction,
	changeFormTypeAction,
	copyStepStateAction,
	addSocialAction,
	resetFormStateAction,
	deleteSocialAciton
} from "./actions";

import { FORM_STATE } from "data/constants";

import { FormProviderProps, Payload } from "./types";

const initFormContext = {
	...FORM_STATE,
	changeValue: (value: Payload) => {},
	changeStep: (value: Payload) => {},
	changeRequired: (value: Payload) => {},
	changeActiveForm: (value: Payload) => {},
	copyStepState: (value: Payload) => {},
	addSocial: (value: Payload) => {},
	setTouch: (value: Payload) => {},
	resetFormState: (value: Payload) => {},
	deleteSocial: (value: Payload) => {},
};

const formContext = createContext(initFormContext);

function FormContextProvider({ children }: FormProviderProps) {
	const [state, dispatch] = useReducer(reducer, FORM_STATE);

	const {
		activeStepIndex,
		steps: { 
			personalInformation,
			individualEntrepreneur,
			limitedLiabilityCompany,
			registrationAddress,
			residentialAddress,
			socialMedia
		},
	} = state;

	const changeValue = useCallback((value: Payload) => {
		dispatch( changeValueAction(value) );
	}, []);

	const changeStep = useCallback((value: Payload) => {
		dispatch( changeStepAction(value) );
	}, []);

	const changeRequired = useCallback((value: Payload) => {
		dispatch( changeRequiredAction(value) )
	}, []);

	const changeActiveForm = useCallback((value: Payload) => {
		dispatch( changeFormTypeAction(value) );
	}, []);

	const copyStepState = useCallback((value: Payload) => {
		dispatch( copyStepStateAction(value) );
	}, []);

	const addSocial = useCallback((value: Payload) => {
		dispatch( addSocialAction(value) );
	}, []);

	const setTouch = useCallback((value: Payload) => {
		dispatch( setTouchAction(value) );
	}, []);

	const resetFormState = useCallback((value: Payload) => {
		dispatch( resetFormStateAction(value) );
	}, []);

	const deleteSocial = useCallback((value: Payload) => {
		dispatch( deleteSocialAciton(value) );
	}, []);

	return (
		<formContext.Provider
			value={{
				activeStepIndex,
				steps: {
					personalInformation,
					individualEntrepreneur,
					limitedLiabilityCompany,
					registrationAddress,
					residentialAddress,
					socialMedia,
				},
				changeValue,
				changeStep,
				changeRequired,
				changeActiveForm,
				copyStepState,
				addSocial,
				setTouch,
				resetFormState,
				deleteSocial,
			}}
		>
			{children}
		</formContext.Provider>
	);
}

export { formContext, FormContextProvider };
