import { produce } from "immer";

import { CHECKS } from "data/checks";
import { ActionKind, FORM_STATE } from "data/constants";

import { changeValidity } from "../utils";
import { isAnObjectPropertyPredicate } from "utils/predicates";

import { FormState, Action } from "../types";

export function reducer(state: FormState, action: Action ): FormState {
	if (action.type === ActionKind.setTouch) {
		if (action.payload.kind === "touch-payload") {
			const { fieldName, stepName, touch, errorsVisible } = action.payload;

			const newState = produce(state, draftState => {
				const step = draftState.steps[stepName];
				step.values[fieldName].touched = touch;
				step.values[fieldName].errorsVisible = errorsVisible;

				changeValidity(step, fieldName, step.values[fieldName].value);
			});

			return newState;
		}
	}

	if (action.type === ActionKind.changeValue) {
		if (action.payload.kind === "value-payload" ) {
			const { fieldName, stepName, value, file } = action.payload;
			
			const newState = produce(state, draftState => {
				const step = draftState.steps[stepName];
				step.values[fieldName].value = value;
				step.values[fieldName].file = file ? file : null;

				changeValidity(step, fieldName, value);
			});
	
			return newState;
		}
	}

	if (action.type === ActionKind.changeRequired) {
		if (action.payload.kind === "required-payload") {
			const { stepName, fieldName } = action.payload;

			const newState = produce(state, draftState => {
				const step = draftState.steps[stepName];
				const isRequired = step.values[fieldName].required;

				step.values[fieldName].required = !isRequired;

				// outsource logic
				const fileds = Object.values(step.values);
				const allValidity = fileds.map( field => field.valid || !field.required );

				const stepIsValid = allValidity.every( validity => validity === true );

				step.valid = stepIsValid;
			});

			return newState;
		}
	}

	if (action.type === ActionKind.changeStep) {
		if (action.payload.kind === "step-payload") {
			const { stepIndex } = action.payload;

			const newState = produce(state, draftState => {
				draftState.activeStepIndex = stepIndex;
			});

			return newState;
		}
	}

	if (action.type === ActionKind.changeFormType) {
		if (action.payload.kind === "form-type-payload") {
			const { activeStepName, disableStepName } = action.payload;

			const newState = produce(state, draftState => {
				if ( isAnObjectPropertyPredicate(draftState.steps, disableStepName) ) {
					draftState.steps[disableStepName] = FORM_STATE.steps[disableStepName];
				}
				if (isAnObjectPropertyPredicate(draftState.steps, activeStepName)) {
					draftState.steps[activeStepName].active = true;
				}
			});

			return newState;
		}
	}

	if (action.type === ActionKind.copyStepState) {
		if (action.payload.kind === "copy-step-state") {
			const { originStepName, destinationStepName } = action.payload;

			const newState = produce(state, draftState => {
				const copyableState = {...draftState.steps[originStepName]};
				const isCopied = draftState.steps[destinationStepName].copied;

				copyableState.type = destinationStepName;
				copyableState.stepDependency = destinationStepName;
				
				if (isCopied) {
					draftState.steps[destinationStepName] = FORM_STATE.steps[destinationStepName];
				}
				else {
					draftState.steps[destinationStepName] = copyableState;
					draftState.steps[destinationStepName].copied = true;
				}
			});

			return newState;
		}
	}

	if (action.type === ActionKind.addSocial) {
		if (action.payload.kind === "add-social-payload") {
			const {
				stepName,
				oldSocialName,
				newSocialName,
				socialValue
			} = action.payload;

			const defaultFieldState = {
				required: true,
				value: socialValue,
				file: null,
				order: 0,
				valid: false,
				touched: false,
				errorsVisible: false,
				errors: [],
				checks: [CHECKS.emptyCheck]
			};

			const newState = produce(state, draftState => {
				const step = draftState.steps[stepName];
				let order = step.values[oldSocialName]?.order;
				if (order === undefined) {
					order = Object.keys(step.values).length;
				}

				step.valid = false;
				step.values[newSocialName] = defaultFieldState;
				step.values[newSocialName].order = order;

				delete step.values[oldSocialName];
			});

			return newState;
		}
	}

	if (action.type === ActionKind.resetFormState) {
		if (action.payload.kind === "reset-form-state-payload") {
			return FORM_STATE;
		}
	}

	if (action.type === ActionKind.deleteSocial) {
		if (action.payload.kind === "delete-social-payload") {
			const { stepName, fieldName } = action.payload;

			const newState = produce(state, draftState => {
				const step = draftState.steps[stepName];
				
				delete step.values[fieldName];

				if (Object.values(step.values).length === 0) {
					step.values["default"] = {
						required: true,
						value: "",
						file: null,
						order: 0,
						valid: false,
						touched: false,
						errorsVisible: false,
						errors: [],
						checks: [CHECKS.emptyCheck],
					}

					step.valid = false;
				}
			});

			return newState;
		}
	}

	return state;
}