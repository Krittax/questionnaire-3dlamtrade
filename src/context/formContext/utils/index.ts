import { WritableDraft } from "immer/dist/internal";
import { Step } from "../types";

export function changeValidity(step: WritableDraft<Step>, fieldName: string, value: string) {
	const checks = step.values[fieldName].checks;
	const errors: string[] = [];

	checks.forEach((check) => {
		if (!check.regexp.test(value)) {
			errors.push(check.errorMessage);
		}
	});

	step.values[fieldName].errors = errors;

	if (errors.length > 0) {
		step.values[fieldName].valid = false;
	} else {
		step.values[fieldName].valid = true;
	}

	const fileds = Object.values(step.values);
	const allValidity = fileds.map( field => field.valid || !field.required );

	const stepIsValid = allValidity.every( validity => validity === true );

	step.valid = stepIsValid;
}
